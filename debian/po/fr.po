# Translation of redmine debconf templates to French
# Copyright (C) 2010 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the redmine package.
#
# Translators:
# Jérémy Lal <jeremy.lal@m4x.org>, 2010.
# Christian Perrier <bubulle@debian.org>, 2010.
# Julien Patriarca <leatherface@debian.org>, 2016
msgid ""
msgstr ""
"Project-Id-Version: redmine 0.9.0-1\n"
"Report-Msgid-Bugs-To: redmine@packages.debian.org\n"
"POT-Creation-Date: 2016-02-15 08:38-0200\n"
"PO-Revision-Date: 2016-02-22 10:22+0100\n"
"Last-Translator: Julien Patriarca <leatherface@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Redmine instances to be configured or upgraded:"
msgstr "Instances Redmine qui seront configurées ou mises à jour :"

#. Type: string
#. Description
#: ../templates:1001
msgid "Space-separated list of instances identifiers."
msgstr ""
"Veuillez indiquer, séparés par des espaces, les identifiants des instances à "
"mettre à jour ou configurer. "

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Each instance has its configuration files in /etc/redmine/<instance-"
"identifier>/"
msgstr ""
"Les fichiers de configuration de chaque instance sont conservés dans /etc/"
"redmine/<identifiant-instance>/."

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"To deconfigure an instance, remove its identifier from this list. "
"Configuration files and data from removed instances will not be deleted "
"until the package is purged, but they will be no longer managed "
"automatically."
msgstr ""
"Pour désinstaller une instance, il faut supprimer son identifiant de cette "
"liste. Les fichiers de configuration ainsi que les données des instances "
"supprimées, ne seront pas effacés tant que le paquet n'a pas été purgé, mais "
"ils ne seront plus gérés automatiquement."

#. Type: select
#. Description
#: ../templates:2001
msgid "Default redmine language:"
msgstr "Langue par défaut de Redmine :"

#~ msgid "redmine-${dbtype} package required"
#~ msgstr "Paquet redmine-${dbtype} indispensable"

#~ msgid ""
#~ "Redmine instance ${instance} is configured to use database type "
#~ "${dbtype}, but the corresponding redmine-${dbtype} package is not "
#~ "installed."
#~ msgstr ""
#~ "L'instance Redmine ${instance} est configurée pour utiliser une base de "
#~ "données de type ${dbtype}, mais le paquet correspondant redmine-${dbtype} "
#~ "n'est pas installé."

#~ msgid "Configuration of instance ${instance} is aborted."
#~ msgstr "La configuration de l'instance ${instance} est interrompue."

#~ msgid ""
#~ "To finish that configuration, please install the redmine-${dbtype} "
#~ "package, and reconfigure redmine using:"
#~ msgstr ""
#~ "Pour terminer cette configuration, veuillez installer le paquet redmine-"
#~ "${dbtype}, puis reconfigurez redmine à l'aide de la commande suivante:"

#~ msgid "dpkg-reconfigure -plow redmine"
#~ msgstr "dpkg-reconfigure -plow redmine"

#~ msgid "Redmine package now supports multiple instances"
#~ msgstr "Gestion de plusieurs instances de Redmine"

#~ msgid ""
#~ "You are migrating from an unsupported version. The current instance will "
#~ "be now called the \"default\" instance. Please check your web server "
#~ "configuration files, see README.Debian."
#~ msgstr ""
#~ "Vous êtes en train de faire migrer Redmine depuis une version non gérée. "
#~ "L'instance actuelle va désormais s'appeler « default ». Veuillez vérifier "
#~ "la configuration du serveur web en vous aidant des indications du "
#~ "fichier /usr/share/doc/redmine/README.Debian."

#~ msgid "Redmine instances to be deconfigured:"
#~ msgstr "Instances Redmine qui seront déconfigurées :"

#~ msgid "Configuration files for these instances will be removed."
#~ msgstr ""
#~ "Les fichiers de configuration pour les instances déconfigurées seront "
#~ "supprimés."

#~ msgid "Database (de)configuration will be asked accordingly."
#~ msgstr ""
#~ "La déconfiguration des bases de données correspondantes sera effectuée."

#~ msgid "To deconfigure an instance, remove its identifier from this list."
#~ msgstr ""
#~ "Pour déconfigurer une instance, il suffit de retirer son identifiant de "
#~ "cette liste."
