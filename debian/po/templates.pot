# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the redmine package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: redmine\n"
"Report-Msgid-Bugs-To: redmine@packages.debian.org\n"
"POT-Creation-Date: 2016-02-15 08:38-0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Redmine instances to be configured or upgraded:"
msgstr ""

#. Type: string
#. Description
#: ../templates:1001
msgid "Space-separated list of instances identifiers."
msgstr ""

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Each instance has its configuration files in /etc/redmine/<instance-"
"identifier>/"
msgstr ""

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"To deconfigure an instance, remove its identifier from this list. "
"Configuration files and data from removed instances will not be deleted "
"until the package is purged, but they will be no longer managed "
"automatically."
msgstr ""

#. Type: select
#. Description
#: ../templates:2001
msgid "Default redmine language:"
msgstr ""
