#!/usr/bin/make -f
# -*- makefile -*-

# We want to run the tests and they run fine,
# except they fail very randomly. :(
export DH_RUBY_IGNORE_TESTS=all

%:
	dh $@ --with ruby

override_dh_auto_clean:
	# tests
	# db/schema.rb is being removed again as this
	# remains if build time tests fail
	rm -rf instances Gemfile.local tmp Gemfile.lock db/schema.rb
	dh_auto_clean

override_dh_auto_configure:
	./debian/check-locales
	bundle install --local --quiet
	rm -f Gemfile.lock

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	mkdir -p instances/default/config/
	cp debian/Gemfile.test.local Gemfile.local
	cp debian/database.test.yml instances/default/config/database.yml
	bin/rake db:migrate RAILS_ENV=test
	bin/rake test RAILS_ENV=test
	rm db/schema.rb
endif

override_dh_install:
	dh_install

	# Clean up the "extra" license files with typos :)
	find debian/redmine/usr/share/redmine -name "*LICEN*E*" -exec rm -f '{}' \;
	# ...and other various files
	find debian/redmine -name '.gitignore' -type f | xargs rm -f

	# remove htacccess example file : complete examples are given in /usr/share/doc/redmine/examples
	rm -f debian/redmine/usr/share/redmine/public/htaccess.fcgi.example

	# running redmine as cgi is too slow
	rm -f debian/redmine/usr/share/redmine/public/dispatch.cgi.example

	# rename cgi script, check permissions
	mv debian/redmine/usr/share/redmine/public/dispatch.fcgi.example debian/redmine/usr/share/redmine/public/dispatch.fcgi

	# remove example config files
	rm -f debian/redmine/usr/share/redmine/config/database.yml.example

	# remove upstream changelog generator script
	rm -f debian/redmine/usr/share/redmine/bin/changelog.rb

	# replace config/email.yml by /etc/redmine/<instance>/email.yml in all locales (Closes: #590846)
	sed -i -e 's/config\/configuration\.yml/\/etc\/redmine\/\&lt\;instance\&gt\;\/configuration\.yml/g' debian/redmine/usr/share/redmine/config/locales/*.yml

	# remove shebang from Rakefile
	sed -i -e '1 { /^#!/ d }' debian/redmine/usr/share/redmine/Rakefile

override_dh_fixperms:
	dh_fixperms
	# fix permissions
	chmod +x debian/redmine/usr/share/redmine/extra/mail_handler/rdm-mailhandler.rb
	chmod -x debian/redmine/usr/share/redmine/app/models/mail_handler.rb
	chmod -x debian/redmine/usr/share/redmine/Rakefile

	# FIXME make redmine-instances executable. This can be dropped when the patch is accepted upstream
	chmod +x debian/redmine/usr/share/redmine/bin/redmine-instances
